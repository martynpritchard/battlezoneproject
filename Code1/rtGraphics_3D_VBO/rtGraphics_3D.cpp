// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <string>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags
#include "src/CGTexturedQuad.h"
#include "src/aiWrapper.h"
#include "src/CGPrincipleAxes.h"
#include "src/texture_loader.h"
#include "src/sceneVBOs.h"


using namespace std;
using namespace CoreStructures;

#pragma region Scene variables and resources

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int								mouse_x, mouse_y;
bool							mDown = false;
bool							wKeyDown, sKeyDown, aKeyDown, dKeyDown = false;

GUClock* mainClock = nullptr;

//
// Main scene resources
//
GUPivotCamera* mainCamera = nullptr;
CGPrincipleAxes* principleAxes = nullptr;
CGTexturedQuad* texturedQuad = nullptr;
const aiScene* aiTank;
const aiScene* ailvl;
const aiScene* aiWall;
const aiScene* aiWall2;
const aiScene* aiWall3;
const aiScene* aiBox1;
const aiScene* aiBox2;
const aiScene* aiBox3;
GLuint TankTexture;
GLuint lvlTexture;
GLuint WallTexture;
GLuint Wall2Texture;
GLuint Wall3Texture;
GLuint Box1Texture;
GLuint Box2Texture;
GLuint Box3Texture;

sceneVBOs* Tank;
sceneVBOs* lvl;
sceneVBOs* Wall;
sceneVBOs* Wall2;
sceneVBOs* Wall3;
sceneVBOs* Box1;
sceneVBOs* Box2;
sceneVBOs* Box3;
GLuint meshShader;

float playerX = 5.0;
float playerY = 5.0;
float playerZ = 5.0;

#pragma endregion


#pragma region Function Prototypes

void init(int argc, char* argv[]); // Main scene initialisation function
void update(void); // Main scene update function
void display(void); // Main scene render function

// Event handling functions
void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void mouseWheel(int wheel, int direction, int x, int y);
void keyDown(unsigned char key, int x, int y);
void closeWindow(void);
void reportContextVersion(void);
void reportExtensions(void);

#pragma endregion


int main(int argc, char* argv[])
{
	init(argc, argv);
	glutMainLoop();

	// Stop clock and report final timing data
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	return 0;
}


void init(int argc, char* argv[]) {

	// Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE/* | GLUT_MULTISAMPLE*/);
	glutSetOption(GLUT_MULTISAMPLE, 4);

	// Setup window
	int windowWidth = 800;
	int windowHeight = 600;
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("Battlezone Clone Project");
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	// Register callback functions
	glutIdleFunc(update); // Main scene update function
	glutDisplayFunc(display); // Main render function
	glutKeyboardFunc(keyDown); // Key down handler
	glutMouseFunc(mouseButtonDown); // Mouse button handler
	glutMotionFunc(mouseMove); // Mouse move handler
	glutMouseWheelFunc(mouseWheel); // Mouse wheel event handler
	glutCloseFunc(closeWindow); // Main resource cleanup handler


	// Initialise glew
	glewInit();

	// Initialise OpenGL...

	wglSwapIntervalEXT(0);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	// Setup main camera
	float viewportAspect = (float)windowWidth / (float)windowHeight;
	mainCamera = new GUPivotCamera(-10.0f, 181.5f, 150.0f, 35.0f, viewportAspect, 0.1f);

	principleAxes = new CGPrincipleAxes();

	texturedQuad = new CGTexturedQuad("..\\Common2\\Resources\\Textures\\glass.jpg");


	//tank code
	aiTank = aiImportModel(string("models\\TankDownloadTest.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Tank = new sceneVBOs(aiTank);
	TankTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Tracks.jpg", TextureProperties(false));
	
	//Map code

	ailvl = aiImportModel(string("models\\GrassMap8.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	
	lvl = new sceneVBOs(ailvl);
	lvlTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Grass.jpg", TextureProperties(false));

	//Wall code

	aiWall = aiImportModel(string("models\\Wall2.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Wall = new sceneVBOs(aiWall);
	WallTexture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Wall.jpg", TextureProperties(false));


	//Wall2 code

	aiWall2 = aiImportModel(string("models\\Wall6.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Wall2 = new sceneVBOs(aiWall2);
	Wall2Texture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Wall.jpg", TextureProperties(false));


	//Wall3 code
	aiWall3 = aiImportModel(string("models\\Wall5.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Wall3 = new sceneVBOs(aiWall3);
	Wall3Texture = fiLoadTexture("..\\Common2\\Resources\\Textures\\Wall.jpg", TextureProperties(false));


	//Box1 code
	aiBox1 = aiImportModel(string("models\\Box6.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Box1 = new sceneVBOs(aiBox1);
	Box1Texture = fiLoadTexture("..\\Common2\\Resources\\Textures\\boxpicture.jpg", TextureProperties(false));

	//Box2 code
	aiBox2 = aiImportModel(string("models\\Box2.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Box2 = new sceneVBOs(aiBox2);
	Box2Texture = fiLoadTexture("..\\Common2\\Resources\\Textures\\boxpicture.jpg", TextureProperties(false));

	//Box3 code
	aiBox3 = aiImportModel(string("models\\Box3.obj"),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);


	Box3 = new sceneVBOs(aiBox3);
	Box3Texture = fiLoadTexture("..\\Common2\\Resources\\Textures\\boxpicture.jpg", TextureProperties(false));

	meshShader = setupShaders(string("shaders\\mesh_shader.vs"), string("shaders\\mesh_shader.fs"));

	// Setup and start the main clock
	mainClock = new GUClock();
}

// Main scene update function (called by FreeGLUT's main event loop every frame) 
void update(void) {

	// Update clock
	mainClock->tick();

	// Redraw the screen
	display();

	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "Battlezone Project - Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	glutSetWindowTitle(timingString);

	//Player movement keys 

	if (wKeyDown == true) {
		playerZ = 1.0f + playerZ;
		wKeyDown = false;
	}
	if (sKeyDown == true) {
		playerZ = -1.0f + playerZ;
		sKeyDown = false;
	}
	if (aKeyDown == true) {
		playerX = 1.1f + playerX;
		aKeyDown = false;
	}
	if (dKeyDown == true) {
		playerX = -1.1f + playerX;
		dKeyDown = false;
	}

}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Set viewport to the client area of the current window
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	// Get view-projection transform as a GUMatrix4
	GUMatrix4 T = mainCamera->projectionTransform() * mainCamera->viewTransform() * GUMatrix4::translationMatrix(-playerX, -playerY, -playerZ);

	if (principleAxes)
		principleAxes->render(T);

	/*if (texturedQuad)
		texturedQuad->render(T);*/

	// Render tank model from obj file
	glBindTexture(GL_TEXTURE_2D, TankTexture);
	glEnable(GL_TEXTURE_2D);
	GUMatrix4 tankMatrix = GUMatrix4::translationMatrix(playerX, playerY, playerZ) * GUMatrix4::scaleMatrix(0.5, 0.5, 0.5);
	GUMatrix4 Tank2 = T * tankMatrix;
	static GLuint mvpLocation = glGetUniformLocation(meshShader, "mvpMatrix");
	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(Tank2.M));
	Tank->render();

	//Map
	glBindTexture(GL_TEXTURE_2D, lvlTexture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));

	lvl->render();



	//Wall
	glBindTexture(GL_TEXTURE_2D, WallTexture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));

	Wall->render(); 


	//Wall2
	glBindTexture(GL_TEXTURE_2D, Wall2Texture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Wall2->render();


	//Wall3
	glBindTexture(GL_TEXTURE_2D, Wall3Texture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Wall3->render();


	//Box1
	glBindTexture(GL_TEXTURE_2D, Box1Texture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Box1->render();

	//Box2
	glBindTexture(GL_TEXTURE_2D, Box2Texture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Box2->render();

	//Box3
	glBindTexture(GL_TEXTURE_2D, Box3Texture);
	glEnable(GL_TEXTURE_2D);

	glUseProgram(meshShader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));
	Box3->render();


	//Transparent Function
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	GUMatrix4 glassMatrix = GUMatrix4::translationMatrix(0.0, 40.0, -80.0) *GUMatrix4::scaleMatrix(100.0, 50.0, 100.0);
	GUMatrix4 newT2 = T * glassMatrix;
	texturedQuad->render(newT2);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	glutSwapBuffers();
}



#pragma region Event handling functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			mDown = false;
		}
	}
}


void mouseMove(int x, int y) {

	int dx = x - mouse_x;
	int dy = y - mouse_y;

	if (mainCamera)
		mainCamera->transformCamera((float)-dy, (float)-dx, 0.0f);

	mouse_x = x;
	mouse_y = y;
}


void mouseWheel(int wheel, int direction, int x, int y) {

	if (mainCamera) {

		if (direction < 0)
			mainCamera->scaleCameraRadius(1.1f);
		else if (direction > 0)
			mainCamera->scaleCameraRadius(0.9f);
	}
}



void keyDown(unsigned char key, int x, int y) {

	// Toggle fullscreen (This does not adjust the display mode however!)
	if (key == 'f')
		glutFullScreenToggle();
	//Settings up movement keys 
	if (key == 'w')
		wKeyDown = true;
	if (key == 's')
		sKeyDown = true;
	if (key == 'a')
		aKeyDown = true;
	if (key == 'd')
		dKeyDown = true;
}


void closeWindow(void) {

	// Clean-up scene resources

	if (mainCamera)
		mainCamera->release();

	if (principleAxes)
		principleAxes->release();

	if (texturedQuad)
		texturedQuad->release();

	//if (exampleModel)
		//exampleModel->release();
}


#pragma region Helper Functions

void reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}

#pragma endregion


#pragma endregion

