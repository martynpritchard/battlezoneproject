#version 460

uniform sampler2D texture;

in vec3 texCoord;
in vec3 brightness;

layout (location=0) out vec4 fragColour;

void main(void) {

  vec4 texColor = texture2D(texture, texCoord.xy)*vec4(brightness, 1.0);
  fragColour = texColor;
    
}