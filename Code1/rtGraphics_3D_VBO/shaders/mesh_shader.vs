#version 460

uniform mat4 mvpMatrix;

layout (location=0) in vec3 vertexPos;
layout (location=1) in vec3 vertexTexCoord;
layout (location=6) in vec3 vertexNormal;

out vec3 texCoord;
out vec3 brightness;

vec3 calcLightContributionForPointLight(vec3 vertexPos, vec3 vertexNormal, vec3 L, vec3 Lcolour, vec3 LK){

vec3 D = L - vertexPos;
float lengthD = length(D);
vec3 D_ = normalize(D);

float a = 1.0/(LK.x + LK.y*lengthD + LK.z*lengthD*lengthD);

float lambertian = clamp(dot(D_, vertexNormal), 0.0, 1.0);

return lambertian * a * Lcolour;

}

void main(void) {

vec3 L = vec3(2.0, 60.0, 0.0);
vec3 Lcol = vec3(1.0, 1.0, 0.0);
vec3 LK1 = vec3(1.0, 0.0, 0.0);

brightness = calcLightContributionForPointLight(vertexPos, vertexNormal, L, Lcol, LK1);

	texCoord = vertexTexCoord;
	gl_Position = mvpMatrix * vec4(vertexPos,1.0);
}
